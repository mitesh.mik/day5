﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5.Model
{
    internal class User
    {
       public string userName;
       public string password;

        public override string ToString()
        {
            return this.userName+" "+this.password;
        }
    }
}
