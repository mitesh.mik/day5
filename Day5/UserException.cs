﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Day5
{
    internal class UserException:Exception
    {
        public UserException()
        {

        }
        public UserException(string Message) :base(Message)
        {

        }
    }
}
