﻿using Day5.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    internal interface IFile
    {
        public void WriteContent(User user,string fiename);
        public void ReadContent(string fiename);

    }
}
