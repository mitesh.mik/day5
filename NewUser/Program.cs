﻿// See https://aka.ms/new-console-template for more information

using NewUser.Exceptions;
using NewUser.Model;
using NewUser.Repository;

UserRepository urepo = new UserRepository();
MyUser user = new MyUser();
Console.WriteLine("enter id");
user.Id = int.Parse(Console.ReadLine());
Console.WriteLine("enter name");
user.Name = Console.ReadLine();
Console.WriteLine("enter city");
user.City = Console.ReadLine();

bool isUserRegister = urepo.RegisterUser(user);
try
{
    if (isUserRegister)
    {
        Console.WriteLine("registration successful");
    }
    else
    {
        throw new MyException("user already exists");
    }
    
}
catch(Exception e)
{
    Console.WriteLine(e.Message);
}

List<string> allvalues=urepo.ReadContent("myUsers.txt");
foreach (string value in allvalues)
{
    Console.WriteLine(value);
}


