﻿using NewUser.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewUser.Repository
{
    internal class UserRepository:IFile
    {
        List<MyUser> users = new List<MyUser>();

        public bool IsUserExists(String username, string filename)
        {
            bool userexist = false;
            using(StreamReader sr = new StreamReader(filename))
            {
                string line;
                while((line = sr.ReadLine())!= null)
                {
                    string[] splitline = line.Split(',');
                    foreach(string s in splitline)
                    {
                        if (s == username)
                        {
                            userexist = true;
                            break;
                           
                        }
                       
                    }

                }
                
            }
            return userexist;

        }

        public List<string> ReadContent(string filename)
        {
         List<string> myvalues = new List<string>();
            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    myvalues.Add(line);
                }
                   
            }
            return myvalues;



        }

        public void WriteContent(MyUser user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))//append
            {
                sw.WriteLine($"{user}");
            }

        }

        public bool RegisterUser(MyUser user)
        {
            users.Add(user);
            string filename = "myUsers.txt";
            string username = user.Name;
            bool isuser=IsUserExists(username, filename);
            if (isuser)
            {
                return false;
            }
            else
            {
                WriteContent(user, filename);
                return true;
            }

        }
    }
}
