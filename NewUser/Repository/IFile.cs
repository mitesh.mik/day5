﻿using NewUser.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewUser.Repository
{
    internal interface IFile
    {
        public void WriteContent(MyUser user, string filename);
        public List<string> ReadContent(string filename);
        public bool IsUserExists(String username,string filename);
    }
}
