﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Model;

namespace User.Repository
{
    internal interface IFile
    {
        public void WriteContent(MyUser user,string filename);
        public void ReadContent(string filename);
    }
}
